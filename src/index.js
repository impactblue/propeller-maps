import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import App from './common/components/App';
import store from './common/store';
import styles from './public/styles/index.styl';

ReactDom.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
