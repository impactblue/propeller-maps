export default (func, interval) => {
    let lastCall = 0;
    let trailTimeout = 0;
    const throttle = (...args) => {
        const now = (new Date()).getTime();
        if (now - lastCall >= interval) {
            func.apply(null, args);
            lastCall = now;
        } else {
            clearTimeout(trailTimeout);
            trailTimeout = setTimeout(throttle.bind.apply(throttle, [null, ...args]), now - lastCall);
        }
    };
    return throttle;
};
