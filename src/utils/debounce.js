export default (func, interval) => {
    let timeout = 0;
    const debounce = (...args) => {
        clearTimeout(timeout);
        timeout = setTimeout(func.bind.apply(func, [null, ...args]), interval);

    };
    return debounce;
};
