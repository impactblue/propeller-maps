export const buildQueryParams = (params: any) => {
    const keyPairs = [];
    for (const key in params) {
        keyPairs.push(`${key}=${params[key]}`);
    }
    return `?${keyPairs.join('&')}`;
}

export const getQueryParams = (query: string) => {
    const keyPairs = query.replace('?', '').split('&');
    const params = {};
    for (let i = 0; i < keyPairs.length; i += 1) {
        const pair = keyPairs[i].split('=');
        params[pair[0]] = pair[1];
    }
    return params;
}
