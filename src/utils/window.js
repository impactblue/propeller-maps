export const getDimensions = () => ({
    width: window.innerWidth,
    height: window.innerHeight
});