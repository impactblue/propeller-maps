const images = {};

const imageContext = require.context('./assets/images/', true);
imageContext.keys().forEach(key => {
    images[key] = imageContext(key);
});

const getImage = (src: string) => images[src];

export default getImage;