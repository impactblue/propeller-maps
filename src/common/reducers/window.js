// @flow

import { SET_WINDOW_SIZE } from '../actions/window';
import type { WindowState, Action } from '../types';
import { getDimensions } from '../../utils/window';

const initialState = getDimensions();

export default (state: WindowState = initialState, action: Action) => {
    switch (action.type) {
        case SET_WINDOW_SIZE:
            return {
                ...state,
                ...action.dimensions
            };
        default:
            return {
                ...state
            };
    }
}