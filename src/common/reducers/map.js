// @flow

import {
    MAP_ZOOM_IN,
    MAP_ZOOM_OUT,
    MAP_SET_POSITION,
    MAP_SET_ZOOM,
    MAP_SET_ZOOM_AND_POSITION,
    MAP_SET_GRID
} from '../actions/map';
import { SET_PUSH_STATE } from '../actions/window';
import { getQueryParams } from '../../utils/query';
import { getDimensions } from '../../utils/window';
import type { MapState, Action, PushState } from '../types';

export const MIN_ZOOM = 1;
export const MAX_ZOOM = 12;

const toBool = (value: any): boolean => value === true || value === 'true';

export const parsePushState = (state: any): PushState => {
    return {
        zoom: parseFloat(state.zoom),
        x: parseFloat(state.x),
        y: parseFloat(state.y),
        grid: toBool(state.grid)
    };
};

const buildInitialState = () => {
    const dimensions = getDimensions();
    const viewPortSize = Math.min(dimensions.width, dimensions.height);

    //Fit minimum zoom to viewport
    const mapSize = {
        x: Math.min(0.8 * viewPortSize),
        y: Math.min(0.8 * viewPortSize)
    };

    const params = parsePushState(getQueryParams(window.location.search));

    return {
        grid: params.grid,
        zoom: params.zoom || 1,
        position: {
            x: params.x || 0,
            y: params.y || 0
        },
        mapSize
    };
};

export default (state: MapState, action: Action): MapState => {
    switch (action.type) {
        case MAP_SET_ZOOM:
            return {
                ...state,
                zoom: Math.min(Math.max(action.zoom, MIN_ZOOM), MAX_ZOOM)
            };
        case MAP_SET_POSITION:
            return {
                ...state,
                position: action.position
            };
        case MAP_SET_ZOOM_AND_POSITION:
            return {
                ...state,
                zoom: Math.min(Math.max(action.zoom, MIN_ZOOM), MAX_ZOOM),
                position: action.position
            }
        case MAP_SET_GRID:
            return {
                ...state,
                grid: action.grid
            };
        case SET_PUSH_STATE:
            return {
                ...state,
                grid: action.state.grid || state.grid,
                zoom: action.state.zoom || state.zoom,
                position: {
                    x: action.state.x || state.position.x,
                    y: action.state.y || state.position.y,
                }
            };
        default:
            if (!state) {
                return buildInitialState();
            }
            return state;
    }
}