import { combineReducers } from 'redux';
import map from './map';
import detail from './detail';
import window from './window';

export default combineReducers({
    map,
    detail,
    window
});