// @flow
import { LOAD_DETAIL_LEVEL } from '../actions/detail';
import type { Action, DetailState, Chunks } from '../types';
import { getQueryParams } from '../../utils/query';

export const DEFAULT_MAP = 'map';
export const ZOOM_SETTINGS = [
    {
        level: 3,
        minZoom: 6,
    },
    {
        level: 2,
        minZoom: 3,
    },
    {
        level: 1,
        minZoom: 1.5,
    },
    {
        level: 0,
        minZoom: 1,
    },
];

const buildInitialState = () => {
    let map = getQueryParams(window.location.search).map;
    return {
        map: map || DEFAULT_MAP,
        levels: {},
        zoomSettings: ZOOM_SETTINGS
    };
};

export default (state: DetailState, action: Action): DetailState => {
    switch (action.type) {
        case LOAD_DETAIL_LEVEL:
            return {
                ...state,
                levels: {
                    ...state.levels,
                    [action.detail.level]: action.detail
                }
            };
        default:
            if (!state) {
                return buildInitialState();
            }
            return state;
    }
}
