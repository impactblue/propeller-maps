import React, { Component, WheelEvent } from 'react';

import type { AppState, TouchEvent } from '../types';

type Props = {
    onTouchStart: (event: TouchEvent) => any,
    onTouchMove: (event: TouchEvent) => any,
    onTouchEnd: (event: TouchEvent) => any,
    onZoom: (delta: number) => any
};

export default class TouchListener extends Component<Props> {
    lastEvent: MouseEvent;
    lastTouchEvents: {
        [number]: Touch;
    };

    constructor(props: Props) {
        super(props);
        this.lastTouchEvents = {};
    }

    componentDidMount() {
        if ('ontouchstart' in window) {
            window.ontouchstart = this.onTouchStart;
            window.ontouchmove = this.onTouchMove;
            window.ontouchend = this.onTouchEnd;
            // Prevent bouncing in mobile browsers
            document.ontouchstart = (event) => event.preventDefault();
        }
        else {
            window.onmousedown = this.onMouseDown;
            window.onmousemove = this.onMouseMove;
            window.onmouseup = this.onMouseUp;
            window.onwheel = this.onWheel;
        }
    }

    componentWillUnmount() {
        window.onmousedown = null;
        window.onmousemove = null;
        window.onmouseup = null;
        window.onwheel = null;
    }

    mouseToTouchEvent(event: MouseEvent) {
        return {
            position: {
                x: event.x,
                y: event.y
            },
            delta: {
                x: this.lastEvent ? event.x - this.lastEvent.x : 0,
                y: this.lastEvent ? event.y - this.lastEvent.y : 0
            }
        };
    }

    touchToTouchEvent(touch: Touch) {
        const lastTouch = this.lastTouchEvents[touch.identifier];
        return {
            position: {
                x: touch.clientX,
                y: touch.clientY
            },
            delta: {
                x: lastTouch ? touch.clientX - lastTouch.clientX : 0,
                y: lastTouch ? touch.clientY - lastTouch.clientY : 0
            }
        };
    }

    onTouchStart = (event: TouchEvent) => {
        for (let i = 0; i < event.changedTouches.length; i += 1) {
            const touch = event.changedTouches[i];
            this.lastTouchEvents[touch.identifier] = touch;
        };
    }

    onTouchMove = (event: TouchEvent) => {
        switch (event.touches.length) {
            case 0:
                break;
            case 1:
                if (this.props.onTouchMove) {
                    const touch = event.changedTouches[0];
                    const touchEvent = this.touchToTouchEvent(touch);
                    this.props.onTouchMove(touchEvent);
                    console.log(touchEvent.delta);
                    this.lastTouchEvents[touch.identifier] = touch;
                }
                break;
            default:
                if (this.props.onZoom) {
                    const oldTouch1 = this.lastTouchEvents[event.touches[0].identifier];
                    const oldTouch2 = this.lastTouchEvents[event.touches[1].identifier];
                    const oldDelta = Math.sqrt(
                        Math.pow(oldTouch1.clientX - oldTouch2.clientX, 2) +
                        Math.pow(oldTouch1.clientY - oldTouch2.clientY, 2)
                    );

                    const newTouch1 = event.touches[0];
                    const newTouch2 = event.touches[1];
                    const newDelta = Math.sqrt(
                        Math.pow(newTouch1.clientX - newTouch2.clientX, 2) +
                        Math.pow(newTouch1.clientY - newTouch2.clientY, 2)
                    );

                    this.lastTouchEvents[newTouch1.identifier] = newTouch1;
                    this.lastTouchEvents[newTouch2.identifier] = newTouch2;

                    console.log(newDelta - oldDelta);
                    this.props.onZoom(newDelta - oldDelta);
                }
                break;
        }
    }

    onTouchEnd = (event: TouchEvent) => {
        for (let i = 0; i < event.changedTouches.length; i += 1) {
            const touch = event.changedTouches[i];
            this.lastTouchEvents[touch.identifier] = null;
        };
        for (let i = 0; i < event.touches.length; i += 1) {
            const touch = event.touches[i];
            this.lastTouchEvents[touch.identifier] = touch;
        };
        if (this.props.onTouchEnd && event.touches.length === 0) {
            this.props.onTouchEnd();
        }
    }


    onMouseDown = (event: MouseEvent) => {
        event.preventDefault();
        event.stopPropagation();
        if (this.props.onTouchStart) {
            this.props.onTouchStart(this.mouseToTouchEvent(event));
        }
        this.lastEvent = event;
    }

    onMouseMove = (event: MouseEvent) => {
        event.preventDefault();
        event.stopPropagation();
        if (this.lastEvent) {
            if (this.props.onTouchMove) {
                this.props.onTouchMove(this.mouseToTouchEvent(event));
            }
            this.lastEvent = event;
        }
    }

    onMouseUp = (event: MouseEvent) => {
        event.preventDefault();
        event.stopPropagation();
        if (this.props.onTouchEnd) {
            this.props.onTouchEnd(this.mouseToTouchEvent(event));
        }
        this.lastEvent = null;
    }

    onWheel = (event: WheelEvent) => {
        if (this.props.onZoom) {
            this.props.onZoom(-event.deltaY);
        }
    }

    render() {
        return null;
    }
}
