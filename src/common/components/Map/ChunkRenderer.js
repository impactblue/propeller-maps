// @flow

type Props = {
    src: string;
    onload: () => any;
};

type DrawParams = {
    context: CanvasRenderingContext2D,
    x: number,
    y: number,
    width: number,
    height: number,
    grid: boolean
};

export default class ChunkRenderer {
    src: string;
    image: any;
    loaded: bool;
    onload: () => any;

    constructor({ src, onload }: Props) {
        this.src = src;
        this.loaded = false;
        this.onload = onload;
        this.image = new Image();
        this.image.src = src;
        this.image.onload = this.onImageLoaded;
    }

    onImageLoaded = () => {
        this.loaded = true;
        this.onload();
    }

    draw({ context, x, y, width, height, grid }: DrawParams) {
        if (grid) {
            context.strokeStyle = "#FFFFFF";
            context.lineWidth = 2;
            context.strokeRect(x, y, width + 2, height + 2);
        }
        if (this.loaded) {
            context.drawImage(this.image, x, y, width, height);
        } else {
            context.fillStyle = '#999999';
            context.fillRect(x, y, width, height);
        }
    }
}