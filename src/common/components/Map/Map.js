// @flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Controls from '../Controls';
import ChunkRenderer from './ChunkRenderer';
import TouchListener from '../TouchListener';
import { setZoomAndPosition, setZoom } from '../../actions/map';
import { MIN_ZOOM, MAX_ZOOM } from '../../reducers/map';
import { getDetailLevelAtZoom } from '../../selectors/detail'
import throttle from '../../../utils/throttle';

import type { AppState, MapState, WindowState, Detail, Vector2, TouchEvent } from '../../types';

import styles from './styles.styl';
import getImage from '../../../public/images';

const PIXELS_PER_ZOOM = 50;
const ANIMATE_ZOOM_DURATION = 200;
const ANIMATION_RATE = 1000 / 60;

type Props = {
    map: MapState,
    window: WindowState,
    detail: Detail,
    dispatch: Dispatch
};

type State = {
    position: Vector2,
    zoom: number,
    detail: Detail
};

export class Map extends Component<Props, State> {
    canvas: HTMLCanvasElement;
    renderers: {
        [string]: ChunkRenderer;
    };
    throttleSetZoom: (zoom: number) => any;
    animateTimer: number;

    constructor(props: Props) {
        super(props);
        this.state = {
            position: props.map.position,
            zoom: props.map.zoom,
            detail: props.detail
        };
        this.renderers = {};
        this.throttleSetZoom = throttle(this.setZoom, 200);
    }

    componentWillReceiveProps(nextProps: Props) {
        this.setState({
            position: nextProps.map.position,
            zoom: nextProps.map.zoom,
            detail: nextProps.detail || this.state.detail
        });
    }

    componentDidUpdate() {
        this.draw();
    }

    draw() {
        if (!this.state.detail) {
            this.drawEmpty();
            return;
        }
        const {
            window: { width, height }
        } = this.props;
        const { zoom, position } = this.state;

        // Apply zoom to base map size
        const mapSize = {
            x: zoom * this.props.map.mapSize.x,
            y: zoom * this.props.map.mapSize.y
        };

        // Calculate position of map in viewport
        const viewportCenter = {
            x: width / 2 + zoom * position.x,
            y: height / 2 + zoom * position.y,
        };

        // Repeat the map if it is smaller than the viewport
        const leadingRows = Math.max(0, Math.ceil((viewportCenter.x - mapSize.x / 2) / mapSize.x));
        const trailingRows = Math.max(0, Math.ceil((width - (viewportCenter.x + mapSize.x / 2)) / mapSize.x));
        const leadingColumns = Math.max(0, Math.ceil((viewportCenter.y - mapSize.y / 2) / mapSize.y));
        const trailingColumns = Math.max(0, Math.ceil((height - (viewportCenter.y + mapSize.y / 2)) / mapSize.y));

        const rows = 1 + leadingRows + trailingRows;
        const columns = 1 + leadingColumns + trailingColumns;

        const start = {
            x: viewportCenter.x - mapSize.x / 2 - leadingRows * mapSize.x,
            y: viewportCenter.y - mapSize.y / 2 - leadingColumns * mapSize.y,
        };

        for (let row = 0; row < rows; row += 1) {
            for (let column = 0; column < columns; column += 1) {
                this.drawMap(
                    {
                        x: start.x + mapSize.x * row,
                        y: start.y + mapSize.y * column,
                    },
                    mapSize
                );
            }
        }
    }

    drawEmpty() {
        const { window: { width, height } } = this.props;
        const context = this.canvas.getContext('2d');
        context.fillStyle = '#999999';
        context.fillRect(0, 0, width, height);
    }

    drawMap(offset: Vector2, mapSize: Vector2) {
        const { map: { grid }, } = this.props;
        const { detail: { chunks } } = this.state;

        const chunkSize = {
            x: mapSize.x / chunks.length,
            y: mapSize.y / chunks[0].length
        };
        const context = this.canvas.getContext('2d');

        for (let row = 0; row < chunks.length; row += 1) {
            for (let column = 0; column < chunks[row].length; column += 1) {
                const position = {
                    x: offset.x + row * chunkSize.x,
                    y: offset.y + column * chunkSize.y,
                };

                // Do not render chunks which are out of the viewport
                if (this.shouldCullChunk(position, chunkSize)) continue;

                const chunk = this.getRenderer(chunks[row][column]);
                chunk.draw({
                    context,
                    ...position,
                    width: chunkSize.x,
                    height: chunkSize.y,
                    grid
                });
            }
        }
    }

    shouldCullChunk(offset: Vector2, size: Vector2) {
        const { window: { width, height } } = this.props;
        return offset.x + size.x < 0 || offset.y + size.y < 0 || offset.x > width || offset.y > height;
    }

    getRenderer(src: string) {
        if (!this.renderers[src]) {
            this.renderers[src] = new ChunkRenderer({
                src: getImage(src),
                onload: this.onChunkLoaded,
            });
        }
        return this.renderers[src];
    }

    onChunkLoaded = () => {
        this.draw();
    }

    onTouchMove = (touch: TouchEvent) => {
        const { map: { zoom, mapSize } } = this.props;
        const { position } = this.state;
        // Calculation new center position by using the delta of the touch move
        // Clamp the position to single map coordinates
        const newPosition = {
            x: (position.x + touch.delta.x / zoom) % mapSize.x,
            y: (position.y + touch.delta.y / zoom) % mapSize.y,
        };
        this.setState({
            position: newPosition
        });
    }

    onTouchEnd = (touch: TouchEvent) => {
        this.props.dispatch(setZoomAndPosition(this.state.zoom, this.state.position));
    }

    onZoom = (delta: number) => {
        const { zoom } = this.state;
        const newZoom = Math.max(Math.min(zoom + delta / PIXELS_PER_ZOOM, MAX_ZOOM), MIN_ZOOM);
        this.setState({
            zoom: newZoom
        });
        this.throttleSetZoom(newZoom);
    }

    onZoomIn = () => {
        if (this.state.zoom < MAX_ZOOM) {
            this.animateToZoom(this.state.zoom * 2);
        }
    }

    onZoomOut = () => {
        if (this.state.zoom > MIN_ZOOM) {
            this.animateToZoom(this.state.zoom * 0.5);
        }
    }

    animateToZoom = (zoom: number) => {
        clearTimeout(this.animateTimer);
        let time = 0;
        let nextZoom = this.state.zoom;
        const delta = (zoom - nextZoom) / (ANIMATE_ZOOM_DURATION / ANIMATION_RATE);
        const next = () => {
            nextZoom = Math.max(Math.min(nextZoom + delta, MAX_ZOOM), MIN_ZOOM);
            this.setState({
                zoom: nextZoom
            });

            if ((delta > 0 && nextZoom < zoom) ||
                (delta < 0 && nextZoom > zoom)) {
                this.animateTimer = setTimeout(next, ANIMATION_RATE);
            } else {
                this.props.dispatch(setZoom(nextZoom));
            }
        };
        next();
    }

    setZoom = (zoom: number) => {
        clearTimeout(this.animateTimer);
        this.props.dispatch(setZoom(zoom));
    }

    createCanvas = (canvas: any) => {
        if (canvas != null) {
            this.canvas = canvas;
            this.draw();
        }
    }

    render() {
        const { window: { width, height } } = this.props;

        return (
            <div className={styles.Map}>
                <canvas ref={this.createCanvas} width={width} height={height} />
                <TouchListener
                    onTouchMove={this.onTouchMove}
                    onTouchEnd={this.onTouchEnd}
                    onZoom={this.onZoom}
                />
                <Controls
                    onZoomIn={this.onZoomIn}
                    onZoomOut={this.onZoomOut}
                />
            </div>
        );
    }
}

const mapState = (state: AppState, props: Props): Props => ({
    ...props,
    map: state.map,
    window: state.window,
    detail: state.detail.levels[getDetailLevelAtZoom(state.detail, state.map.zoom)],
});

export default connect(mapState)(Map);
