// @flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.styl';

type Props = {
    onClick: () => any,
    children: any,
    styles?: any,
};

class Button extends Component<Props> {
    onTouchStart = (event: SyntheticEvent<*>) => {
        event.nativeEvent.stopImmediatePropagation();
        this.props.onClick();
    }

    stopPropagation = (event: SyntheticEvent<*>) => {
        event.nativeEvent.stopImmediatePropagation();
    }

    render() {
        const { onClick, children } = this.props;
        const useTouch = 'ontouchstart' in window;
        return (
            <button
                className={classnames([styles.Button, this.props.styles])}
                onMouseDown={!useTouch ? this.onTouchStart : null}
                onMouseUp={!useTouch ? this.stopPropagation : null}
                onTouchStart={useTouch ? this.onTouchStart : null}
                onTouchEnd={useTouch ? this.stopPropagation : null}
            >
                {children}
            </button >
        );
    }
}

export default Button;