// @flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from './Button';
import CheckBox from './CheckBox';
import { setGrid } from '../../actions/map';
import type { AppState, MapState } from '../../types';
import styles from './styles.styl';

type Props = {
    grid: boolean,
    onZoomIn: () => any,
    onZoomOut: () => any,
    dispatch: Dispatch
}

export class Controls extends Component<Props> {

    onZoomIn = () => {
        this.props.onZoomIn();
    }

    onZoomOut = () => {
        this.props.onZoomOut();
    }

    toggleGrid = () => {
        this.props.dispatch(setGrid(!this.props.grid));
    }

    render() {
        const { grid } = this.props;
        return (
            <div className={styles.Controls}>
                <Button styles={styles.ZoomButton} onClick={this.onZoomIn}>+</Button>
                <Button styles={styles.ZoomButton} onClick={this.onZoomOut}>-</Button>
                <div>
                    <label className={styles.GridLabel}>Grid</label>
                    <CheckBox styles={styles.ToggleGrid} onClick={this.toggleGrid} checked={grid}></CheckBox>
                </div>
            </div >
        );
    }
}

const mapState = (state: AppState, props: Props): Props => ({
    ...props,
    grid: state.map.grid,
});

export default connect(mapState)(Controls);
