// @flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.styl';

type Props = {
    checked: boolean,
    onClick: () => any,
    styles?: any,
};

class CheckBox extends Component<Props> {
    onTouchStart = (event: SyntheticEvent<*>) => {
        event.nativeEvent.stopImmediatePropagation();
        this.props.onClick();
    }

    stopPropagation = (event: SyntheticEvent<*>) => {
        event.nativeEvent.stopImmediatePropagation();
    }

    render() {
        const { onClick, checked } = this.props;
        const useTouch = 'ontouchstart' in window;
        return (
            <button
                className={classnames([styles.CheckBox, this.props.styles, { [styles.Checked]: checked }])}
                onMouseDown={!useTouch ? this.onTouchStart : null}
                onMouseUp={!useTouch ? this.stopPropagation : null}
                onTouchStart={useTouch ? this.onTouchStart : null}
                onTouchEnd={useTouch ? this.stopPropagation : null}
            >
            </button >
        );
    }
}

export default CheckBox;