import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setWindowSize, onPopState } from '../actions/window';
import throttle from '../../utils/throttle';
import { getDimensions } from '../../utils/window';

import type { AppState } from '../types';

type Props = {
    dispatch: Dispatch
};

export class WindowListener extends Component<Props> {
    componentDidMount() {
        window.onresize = throttle(this.onResize, 200);
        window.onpopstate = this.onPopState;
    }

    componentWillUnmount() {
        window.onresize = null;
    }

    onResize = () => {
        const dimensions = getDimensions();
        this.props.dispatch(setWindowSize(dimensions));
    }

    onPopState = (event: PopStateEvent) => {
        this.props.dispatch(onPopState(event.state));
    }

    render() {
        return null;
    }
}

const mapState = (state: AppState, props: Props) => ({ ...props });

export default connect(mapState)(WindowListener);