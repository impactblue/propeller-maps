// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import Controls from '../Controls';
import Map from '../Map';
import { appInit } from '../../actions/app';
import WindowListener from '../WindowListener';
import styles from './styles.styl';

type Props = {
    dispatch: Dispatch
};

class App extends Component<Props> {
    componentDidMount() {
        this.props.dispatch(appInit());
    }

    render() {
        return (
            <div className={styles.App}>
                <Map />
                <WindowListener />
            </div>
        );
    }
}

export default connect()(App);