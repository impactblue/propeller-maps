// @flow
import { getDetailLevelAtZoom } from '../selectors/detail';
import { loadDetailLevel } from '../actions/detail';
import { APP_INIT } from '../actions/app';
import {
    MAP_SET_ZOOM,
    MAP_SET_ZOOM_AND_POSITION
} from '../actions/map';
import type { AppState, Action, Detail } from '../types';

// Convenience method for building test chunk data
const buildChunks = (name: string, type: string, level: number, rows: number, columns) => {
    const chunks = [];
    for (let row = 0; row < rows; row += 1) {
        for (let column = 0; column < columns; column += 1) {
            if (!chunks[row]) {
                chunks[row] = [];
            }
            chunks[row][column] = `./${name}/${level}/${row}/${column}.${type}`;
        }
    }
    return chunks;
}

export const mapData = {
    map: {
        '3': {
            level: 3,
            chunks: buildChunks('map', 'png', 3, 8, 8)
        },
        '2': {
            level: 2,
            chunks: buildChunks('map', 'png', 2, 4, 4)
        },
        '1': {
            level: 1,
            chunks: buildChunks('map', 'png', 1, 2, 2)
        },
        '0': {
            level: 0,
            chunks: buildChunks('map', 'png', 0, 1, 1)
        },
    },
    propeller: {
        '3': {
            level: 3,
            chunks: buildChunks('propeller', 'jpg', 3, 8, 8)
        },
        '2': {
            level: 2,
            chunks: buildChunks('propeller', 'jpg', 2, 4, 4)
        },
        '1': {
            level: 1,
            chunks: buildChunks('propeller', 'jpg', 1, 2, 2)
        },
        '0': {
            level: 0,
            chunks: buildChunks('propeller', 'jpg', 0, 1, 1)
        },
    }
};

//Mock Api Response
const loadDetail = (map: string, level: number): Promise<Detail> => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(mapData[map][level]);
    }, 100);
});

const detail = ({ getState, dispatch }: Store) => (next: (action: Action) => any) => async (action: Action) => {
    await next(action);
    switch (action.type) {
        case APP_INIT:
        case MAP_SET_ZOOM:
        case MAP_SET_ZOOM_AND_POSITION:
            const state = getState();
            const zoom = state.map.zoom;
            const level = getDetailLevelAtZoom(state.detail, zoom);
            const detail = state.detail.levels[level];
            if (!detail) {
                const newDetail = await loadDetail(state.detail.map, level);
                dispatch(loadDetailLevel(newDetail));
            }
        default:
            break;
    }
}

export default detail;
