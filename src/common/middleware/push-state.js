// @flow
import { pushState as historyPushState } from '../../utils/history';

import {
    MAP_SET_GRID,
    MAP_SET_POSITION,
    MAP_SET_ZOOM,
    MAP_SET_ZOOM_AND_POSITION
} from '../actions/map';
import { buildQueryParams } from '../../utils/query';
import type { AppState, PushState, Action } from '../types';

export const getRouteParams = (state: AppState): PushState => {
    return {
        map: state.detail.map,
        x: state.map.position.x,
        y: state.map.position.y,
        zoom: state.map.zoom,
        grid: state.map.grid
    };
}

const pushState = ({ getState }: Store) => (next: (action: Action) => any) => async (action: Action) => {
    switch (action.type) {
        case MAP_SET_POSITION:
        case MAP_SET_ZOOM:
        case MAP_SET_GRID:
        case MAP_SET_ZOOM_AND_POSITION:
            const nextState = await next(action);
            const state = getState();
            const params = getRouteParams(state);
            historyPushState(params, 'Maps', `/${buildQueryParams(params)}`);
            return nextState;
        default:
            return next(action);
    }
}

export default pushState;
