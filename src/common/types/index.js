export type Vector2 = {
    x: number,
    y: number
};

export type Detail = {
    level: number,
    chunks: string[][]
};

export type ZoomSetting = {
    level: number,
    minZoom: number
};

export type TouchEvent = {
    delta: Vector2,
    position: Vector2
};

export type MapState = {
    zoom: number,
    position: Vector2,
    mapSize: Vector2,
    grid: boolean
};

export type PushState = {
    map: string,
    zoom: string,
    x: string,
    y: string,
    grid: boolean
};

export type DetailState = {
    map: string,
    levels: {
        [level: number]: Detail
    },
    zoomSettings: ZoomSetting[]
};

export type WindowState = {
    width: number,
    height: number
};

export type AppState = {
    map: MapState,
    detail: DetailState,
    window: WindowState
};

export type Action = {
    type: string,
};
