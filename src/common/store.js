import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import pushState from './middleware/push-state';
import detail from './middleware/detail';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducers,
    composeEnhancers(
        applyMiddleware(pushState, detail)
    )
);

export default store;