import type { DetailState } from '../types';

export const getDetailLevelAtZoom = (detail: DetailState, zoom: number): number => {
    for (let i = 0; i < detail.zoomSettings.length; i += 1) {
        if (zoom >= detail.zoomSettings[i].minZoom) {
            return detail.zoomSettings[i].level;
        }
    }
    return 0;
}