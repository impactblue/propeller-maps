// @flow

import type { PushState } from '../types';
export const SET_WINDOW_SIZE = "SET_WINDOW_SIZE";
export const SET_PUSH_STATE = "SET_PUSH_STATE";

export const setWindowSize = (dimensions: { width: number, height: number }) => ({
    type: SET_WINDOW_SIZE,
    dimensions
});

export const onPopState = (state: PushState) => ({
    type: SET_PUSH_STATE,
    state
});
