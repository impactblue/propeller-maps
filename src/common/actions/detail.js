export const LOAD_DETAIL_LEVEL = 'LOAD_DETAIL_LEVEL';

export const loadDetailLevel = (detail: DetailState) => ({
    type: LOAD_DETAIL_LEVEL,
    detail
});