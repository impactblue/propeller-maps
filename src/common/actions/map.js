// @flow
import type { Vector2 } from '../types';

export const MAP_ZOOM_IN = "MAP_ZOOM_IN";
export const MAP_ZOOM_OUT = "MAP_ZOOM_OUT";
export const MAP_SET_POSITION = "MAP_SET_POSITION";
export const MAP_SET_ZOOM = "MAP_SET_ZOOM";
export const MAP_SET_ZOOM_AND_POSITION = "MAP_SET_ZOOM_AND_POSITION";
export const MAP_SET_GRID = "MAP_SET_GRID";

export const zoomIn = () => ({
    type: MAP_ZOOM_IN
});

export const zoomOut = () => ({
    type: MAP_ZOOM_OUT
});

export const setPosition = (position: Vector2) => ({
    type: MAP_SET_POSITION,
    position
});

export const setZoom = (zoom: number) => ({
    type: MAP_SET_ZOOM,
    zoom
});

export const setZoomAndPosition = (zoom: number, position: Vector2) => ({
    type: MAP_SET_ZOOM_AND_POSITION,
    zoom,
    position
});

export const setGrid = (grid: boolean) => ({
    type: MAP_SET_GRID,
    grid
});