# Propeller Aero Map Coding Challenge

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

How to install [yarn](https://www.npmjs.com/package/yarn/tutorial).

### Installing

Run yarn to install dependencies and build the project:

```
yarn
```

## Running the project

To start webpack dev server run:

```
yarn start
```

To run with the default map go to [http://localhost:8080](http://localhost:8080).

To run maps with the original drone image, set the `map` query string to `propeller` [http://localhost:8080?map=propeller](http://localhost:8080?map=propeller).

Maps is also hosted on [Heroku](http://propeller-maps.herokuapp.com).

## Running the tests

Run the Jest unit tests with:

```
yarn test
```

## Running flow

Run flow type check with:
```
yarn flow
```

## Deployment

To deploy Maps ensure the project is built with:

```
yarn build
```

Then start the express server with:

```
node server.js
```