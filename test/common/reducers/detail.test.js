import detail, { ZOOM_SETTINGS } from '../../../src/common/reducers/detail';
import { LOAD_DETAIL_LEVEL } from '../../../src/common/actions/detail';
import { getQueryParams } from '../../../src/utils/query';

jest.mock('../../../src/utils/query', () => ({
    getQueryParams: jest.fn()
}));

describe('detailReducer', () => {
    describe('initialState', () => {
        it('should return correct data for map specified in query string', () => {
            getQueryParams.mockReturnValue({
                map: 'propeller'
            });
            expect(detail(undefined, {})).toEqual({
                map: 'propeller',
                levels: {},
                zoomSettings: ZOOM_SETTINGS
            });
        });
    });

    describe('LOAD_DETAIL_LEVEL', () => {
        it('should add detail level', () => {
            expect(detail(
                {
                    levels: {}
                },
                {
                    type: LOAD_DETAIL_LEVEL,
                    detail: {
                        level: 0,
                        chunks: 'chunks'
                    }
                })).toEqual({
                    levels: {
                        0: {
                            level: 0,
                            chunks: 'chunks'
                        }
                    }
                });
        });
    });
});
