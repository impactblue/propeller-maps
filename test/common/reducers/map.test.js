import map, { MIN_ZOOM, MAX_ZOOM } from '../../../src/common/reducers/map';
import { MAP_SET_ZOOM, MAP_SET_POSITION } from '../../../src/common/actions/map';
import { SET_PUSH_STATE } from '../../../src/common/actions/window';
import { getDimensions } from '../../../src/utils/window';
import { getQueryParams } from '../../../src/utils/query';

jest.mock('../../../src/utils/query', () => ({
    getQueryParams: jest.fn()
}));

jest.mock('../../../src/utils/window', () => ({
    getDimensions: jest.fn()
}));

describe('mapReducer', () => {
    describe('initialState', () => {
        it('should correctly set base map size from screen dimensions', () => {
            getQueryParams.mockImplementation(() => ({}));
            getDimensions.mockImplementation(() => ({
                width: 100,
                height: 100
            }));

            expect(map(undefined, {})).toEqual({
                zoom: 1,
                grid: false,
                position: {
                    x: 0,
                    y: 0
                },
                mapSize: {
                    x: 80,
                    y: 80
                }
            });
        });

        it('should correctly load initial state from query strings', () => {
            getQueryParams.mockImplementation(() => ({
                zoom: '2.2',
                x: '5.5',
                y: '6.6',
                grid: 'true'
            }));
            getDimensions.mockImplementation(() => ({
                width: 1000,
                height: 1000
            }));

            expect(map(undefined, {})).toEqual({
                zoom: 2.2,
                grid: true,
                position: {
                    x: 5.5,
                    y: 6.6
                },
                mapSize: {
                    x: 800,
                    y: 800
                }
            })
        })
    });

    describe('MAP_SET_ZOOM', () => {
        it('should clamp and set zoom', () => {
            expect(map({}, {
                type: MAP_SET_ZOOM,
                zoom: 0.5 * (MAX_ZOOM + MIN_ZOOM)
            })).toEqual({
                zoom: 0.5 * (MAX_ZOOM + MIN_ZOOM)
            });
        });

        it('should clamp and set zoom', () => {
            expect(map({}, {
                type: MAP_SET_ZOOM,
                zoom: MIN_ZOOM - 1
            })).toEqual({
                zoom: MIN_ZOOM
            });

            expect(map({}, {
                type: MAP_SET_ZOOM,
                zoom: MAX_ZOOM + 1
            })).toEqual({
                zoom: MAX_ZOOM
            });
        });
    });

    describe('MAP_SET_POSITION', () => {
        it('should set position', () => {
            expect(map({}, {
                type: MAP_SET_POSITION,
                position: { x: 10, y: 15 }
            })).toEqual({
                position: { x: 10, y: 15 }
            });
        });
    });

    describe('SET_PUSH_STATE', () => {
        it('should update zoom, position and grid from push state', () => {
            expect(map(
                {
                    zoom: 1,
                    grid: false,
                    position: {
                        x: 0,
                        y: 0
                    }
                },
                {
                    type: SET_PUSH_STATE,
                    state: {
                        zoom: 5,
                        grid: true,
                        x: 15,
                        y: 20
                    }
                })
            ).toEqual({
                zoom: 5,
                grid: true,
                position: {
                    x: 15,
                    y: 20
                }
            });
        });

        it('should not override state if new state is empty', () => {
            expect(map(
                {
                    zoom: 1,
                    grid: false,
                    position: {
                        x: 0,
                        y: 0
                    }
                },
                {
                    type: SET_PUSH_STATE,
                    state: {}
                })
            ).toEqual({
                zoom: 1,
                grid: false,
                position: {
                    x: 0,
                    y: 0
                }
            });
        });
    });
});
