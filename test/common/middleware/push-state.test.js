import pushState from '../../../src/common/middleware/push-state';
import {
    MAP_SET_GRID,
    MAP_SET_POSITION,
    MAP_SET_ZOOM,
    MAP_SET_ZOOM_AND_POSITION
} from '../../../src/common/actions/map';
import * as history from '../../../src/utils/history';

jest.mock('../../../src/utils/history', () => ({
    pushState: jest.fn()
}));

const createStore = ({ map }) => ({
    getState: jest.fn().mockReturnValue({
        detail: {
            map: 'map'
        },
        map: {
            grid: false,
            zoom: 1,
            position: {
                x: 0,
                y: 0
            },
            ...map
        }
    })
});

describe('pushStateMiddleware', () => {
    it('should push new state when grid is changed', async () => {
        const mockStore = createStore({ map: { grid: true } });
        const next = jest.fn();
        await pushState(mockStore)(next)({ type: MAP_SET_GRID, grid: true });
        expect(next).toBeCalledWith({ type: MAP_SET_GRID, grid: true });
        expect(history.pushState).toBeCalledWith({
            map: 'map',
            grid: true,
            zoom: 1,
            x: 0,
            y: 0
        }, 'Maps', '/?map=map&x=0&y=0&zoom=1&grid=true');
    });

    it('should push new state when zoom is changed', async () => {
        const mockStore = createStore({ map: { zoom: 2 } });
        const next = jest.fn();
        await pushState(mockStore)(next)({ type: MAP_SET_ZOOM, zoom: 2 });
        expect(next).toBeCalledWith({ type: MAP_SET_ZOOM, zoom: 2 });
        expect(history.pushState).toBeCalledWith({
            map: 'map',
            grid: false,
            zoom: 2,
            x: 0,
            y: 0
        }, 'Maps', '/?map=map&x=0&y=0&zoom=2&grid=false');
    });

    it('should push new state when zoom and position is changed', async () => {
        const mockStore = createStore({ map: { zoom: 2, position: { x: 10, y: 15 } } });
        const next = jest.fn();
        await pushState(mockStore)(next)({ type: MAP_SET_ZOOM_AND_POSITION, zoom: 2, position: { x: 10, y: 15 } });
        expect(next).toBeCalledWith({ type: MAP_SET_ZOOM_AND_POSITION, zoom: 2, position: { x: 10, y: 15 } });
        expect(history.pushState).toBeCalledWith({
            map: 'map',
            grid: false,
            zoom: 2,
            x: 10,
            y: 15
        }, 'Maps', '/?map=map&x=10&y=15&zoom=2&grid=false');
    });

    it('should push new state when MAP_SET_POSITION is changed', async () => {
        const mockStore = createStore({ map: { position: { x: 10, y: 15 } } });
        const next = jest.fn();
        await pushState(mockStore)(next)({ type: MAP_SET_POSITION, position: { x: 10, y: 15 } });
        expect(next).toBeCalledWith({ type: MAP_SET_POSITION, position: { x: 10, y: 15 } });
        expect(history.pushState).toBeCalledWith({
            map: 'map',
            grid: false,
            zoom: 1,
            x: 10,
            y: 15
        }, 'Maps', '/?map=map&x=10&y=15&zoom=1&grid=false');
    });
});