import detail, { mapData } from '../../../src/common/middleware/detail';
import detailReducer from '../../../src/common/reducers/detail';
import { LOAD_DETAIL_LEVEL } from '../../../src/common/actions/detail';
import { MAP_SET_ZOOM } from '../../../src/common/actions/map';
import { APP_INIT } from '../../../src/common/actions/app';

describe('detailMiddleware', () => {
    it('should load map data for zoom on app init', async () => {
        const mockStore = {
            getState: jest.fn().mockReturnValue({
                detail: detailReducer(undefined, {}),
                map: {
                    zoom: 1
                }
            }),
            dispatch: jest.fn()
        };

        const next = jest.fn();
        const action = await detail(mockStore)(next)({ type: APP_INIT });
        expect(mockStore.dispatch).toBeCalledWith({
            type: LOAD_DETAIL_LEVEL,
            detail: mapData['map'][0]
        });
    });

    it('should load map data for new zoom', async () => {
        const mockStore = {
            getState: jest.fn().mockReturnValue({
                detail: detailReducer(undefined, {}),
                map: {
                    zoom: 2
                }
            }),
            dispatch: jest.fn()
        };

        const next = jest.fn();
        const action = await detail(mockStore)(next)({
            type: MAP_SET_ZOOM,
            zoom: 2
        });

        expect(next).toBeCalledWith({
            type: MAP_SET_ZOOM,
            zoom: 2
        });
        expect(mockStore.dispatch).toBeCalledWith({
            type: LOAD_DETAIL_LEVEL,
            detail: mapData['map'][1]
        });
    });
});