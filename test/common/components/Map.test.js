import React from 'react';
import { shallow } from 'enzyme';
import { Map } from '../../../src/common/components/Map/Map';

jest.mock('../../../src/public/images', () => (image) => image);

const buildMap = (customProps) => {
    const props = {
        window: { width: 100, height: 100 },
        map: { grid: true, zoom: 1, position: { x: 0, y: 0 }, mapSize: { x: 50, y: 50 } },
        detail: {
            chunks: [
                ['0_0.jpg', '0_1.jpg'],
                ['1_0.jpg', '1_1.jpg']
            ]
        },
        ...customProps
    };
    return {
        wrapper: shallow(<Map {...props} />),
        props
    };
};

describe('Map', () => {
    describe('draw', () => {
        it('should draw an empty map when no chunk data available', () => {
            const { wrapper } = buildMap({
                detail: null
            });
            const map = wrapper.instance();
            map.drawEmpty = jest.fn();
            map.draw();
            expect(map.drawEmpty).toBeCalled();
        });

        it('should draw repeating maps', () => {
            const { wrapper } = buildMap();
            const map = wrapper.instance();
            map.drawMap = jest.fn();
            map.draw();
            expect(map.drawMap).toBeCalledWith(
                { x: -25, y: -25 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 25, y: -25 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 75, y: -25 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: -25, y: 25 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 25, y: 25 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 75, y: 25 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: -25, y: 75 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 25, y: 75 },
                { x: 50, y: 50 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 75, y: 75 },
                { x: 50, y: 50 }
            );

        });

        it('should correct render map in viewport when zoomed and panned', () => {
            const { wrapper } = buildMap({
                map: {
                    zoom: 2,
                    position: { x: 10, y: 10 },
                    mapSize: { x: 50, y: 50 }
                }
            });
            const map = wrapper.instance();
            map.drawMap = jest.fn();
            map.draw();
            expect(map.drawMap).toBeCalledWith(
                { x: 20, y: 20 },
                { x: 100, y: 100 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: -80, y: 20 },
                { x: 100, y: 100 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: 20, y: -80 },
                { x: 100, y: 100 }
            );
            expect(map.drawMap).toBeCalledWith(
                { x: -80, y: -80 },
                { x: 100, y: 100 }
            );
        });
    });

    describe('drawMap', () => {
        it('should draw chunks with correct params', () => {
            const { wrapper } = buildMap();
            const map = wrapper.instance();
            const mockRenderers = {};
            //Build a new mock chunk for each unique src
            map.getRenderer = jest.fn(src => {
                mockRenderers[src] = {
                    draw: jest.fn()
                };
                return mockRenderers[src];
            });
            map.canvas = {
                getContext: jest.fn().mockReturnValue('context')
            };
            map.drawMap(
                {
                    x: 10,
                    y: 10
                },
                {
                    x: 50,
                    y: 50
                }
            );

            expect(map.getRenderer).toBeCalledWith('0_0.jpg');
            expect(map.getRenderer).toBeCalledWith('1_0.jpg');
            expect(map.getRenderer).toBeCalledWith('0_1.jpg');
            expect(map.getRenderer).toBeCalledWith('1_1.jpg');
            expect(mockRenderers['0_0.jpg'].draw).toBeCalledWith({
                context: 'context',
                x: 10,
                y: 10,
                width: 25,
                height: 25,
                grid: true
            });
            expect(mockRenderers['1_0.jpg'].draw).toBeCalledWith({
                context: 'context',
                x: 35,
                y: 10,
                width: 25,
                height: 25,
                grid: true
            });
            expect(mockRenderers['0_1.jpg'].draw).toBeCalledWith({
                context: 'context',
                x: 10,
                y: 35,
                width: 25,
                height: 25,
                grid: true
            });
            expect(mockRenderers['1_1.jpg'].draw).toBeCalledWith({
                context: 'context',
                x: 35,
                y: 35,
                width: 25,
                height: 25,
                grid: true
            });
        });
    });

    describe('shouldCullChunk', () => {
        it('should cull a chunk which is partially in viewport', () => {
            const { wrapper } = buildMap();
            const map = wrapper.instance();
            expect(map.shouldCullChunk(
                { x: -10, y: 0 },
                { x: 20, y: 20 }
            )).toBeFalsy();
            expect(map.shouldCullChunk(
                { x: 0, y: -10 },
                { x: 20, y: 20 }
            )).toBeFalsy();
            expect(map.shouldCullChunk(
                { x: 90, y: 0 },
                { x: 20, y: 20 }
            )).toBeFalsy();
            expect(map.shouldCullChunk(
                { x: 0, y: 90 },
                { x: 20, y: 20 }
            )).toBeFalsy();
        });

        it('should cull a chunk which is completely out of viewport', () => {
            const { wrapper } = buildMap();
            const map = wrapper.instance();
            expect(map.shouldCullChunk(
                { x: -30, y: 0 },
                { x: 20, y: 20 }
            )).toBeTruthy();
            expect(map.shouldCullChunk(
                { x: 0, y: -30 },
                { x: 20, y: 20 }
            )).toBeTruthy();
            expect(map.shouldCullChunk(
                { x: 110, y: 0 },
                { x: 20, y: 20 }
            )).toBeTruthy();
            expect(map.shouldCullChunk(
                { x: 0, y: 110 },
                { x: 20, y: 20 }
            )).toBeTruthy();
        });
    });

    describe('onTouchMove', () => {
        it('should change to correct position', () => {
            const { wrapper } = buildMap({
                map: {
                    zoom: 2, position: { x: 10, y: 10 }, mapSize: { x: 50, y: 50 }
                }
            });
            const map = wrapper.instance();
            map.setState = jest.fn();
            map.onTouchMove({
                delta: {
                    x: 20,
                    y: 10
                }
            });
            expect(map.setState).toBeCalledWith({
                position: {
                    x: 20,
                    y: 15
                }
            })
        });

        it('should wrap position to local map coordinates', () => {
            const { wrapper } = buildMap({
                map: {
                    zoom: 2, position: { x: 10, y: 10 }, mapSize: { x: 50, y: 50 }
                }
            });
            const map = wrapper.instance();
            map.setState = jest.fn();
            map.onTouchMove({
                delta: {
                    x: 90,
                    y: 80
                }
            });
            expect(map.setState).toBeCalledWith({
                position: {
                    x: 5,
                    y: 0
                }
            });
        });
    });

    describe('onZoom', () => {
        it('should change zoom by correct amount', () => {
            const { wrapper } = buildMap({
                map: {
                    zoom: 2, position: { x: 10, y: 10 }, mapSize: { x: 50, y: 50 }
                }
            });
            const map = wrapper.instance();
            map.setState = jest.fn();
            map.throttleSetZoom = jest.fn();
            map.onZoom(100);
            expect(map.setState).toBeCalledWith({
                zoom: 4
            });
            expect(map.throttleSetZoom).toBeCalledWith(4);
        });
    });
});