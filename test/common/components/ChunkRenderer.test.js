import ChunkRenderer from '../../../src/common/components/Map/ChunkRenderer';

const buildContext = () => ({
    strokeRect: jest.fn(),
    drawImage: jest.fn(),
    fillRect: jest.fn()
});

describe('ChunkRenderer', () => {
    describe('draw', () => {
        it('should draw grid if grid option is true', () => {
            const renderer = new ChunkRenderer({
                src: 'image'
            });
            const context = buildContext();
            renderer.draw({
                context,
                x: 10,
                y: 10,
                width: 100,
                height: 100,
                grid: true
            });
            expect(context.strokeStyle).toEqual('#FFFFFF');
            expect(context.lineWidth).toEqual(2);
            expect(context.strokeRect).toBeCalledWith(10, 10, 102, 102);
        });

        it('should draw placeholder if image is not loaded', () => {
            const renderer = new ChunkRenderer({
                src: 'image'
            });
            const context = buildContext();
            renderer.draw({
                context,
                x: 10,
                y: 10,
                width: 100,
                height: 100,
                grid: true
            });
            expect(context.fillStyle).toEqual('#999999');
            expect(context.fillRect).toBeCalledWith(10, 10, 100, 100);
        });

        it('should draw image if loaded', () => {
            const renderer = new ChunkRenderer({
                src: 'image'
            });
            renderer.loaded = true;
            const context = buildContext();
            renderer.draw({
                context,
                x: 10,
                y: 10,
                width: 100,
                height: 100,
                grid: true
            });
            expect(context.drawImage).toBeCalledWith(renderer.image, 10, 10, 100, 100);
        });
    });

    describe('onImageLoaded', () => {
        it('should set loaded to true and call onload when image is loaded', () => {
            const onload = jest.fn();
            const renderer = new ChunkRenderer({
                src: 'image',
                onload
            });
            renderer.image.onload();
            expect(renderer.loaded).toBeTruthy();
            expect(onload).toBeCalled();
        });
    });
});