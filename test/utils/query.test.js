import { buildQueryParams, getQueryParams } from '../../src/utils/query';

describe('query utils', () => {
    describe('buildQueryParams', () => {
        it('should convert an object into a query string', () => {
            expect(buildQueryParams({
                map: 'testmap',
                x: 10,
                y: 15,
                zoom: 3
            })).toEqual('?map=testmap&x=10&y=15&zoom=3');
        })
    });

    describe('getQueryParams', () => {
        it('should convert a query string to an object containing key and values', () => {
            expect(getQueryParams('?map=testmap&x=10&y=15&zoom=3')).toEqual({
                map: 'testmap',
                x: '10',
                y: '15',
                zoom: '3'
            });
        });
    });
});