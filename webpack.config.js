const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/public/index.html',
    filename: 'index.html',
    inject: 'body'
})
const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('dist'),
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: [
                        "react",
                        "flow",
                        "es2015"
                    ],
                    plugins: [
                        "transform-object-rest-spread",
                        "transform-class-properties"
                    ]
                }
            },
            {
                test: /\.styl$/,
                loader: 'style-loader'
            },
            {
                test: /\.styl$/,
                loader: 'css-loader',
                query: {
                    modules: true,
                    localIdentName: '[name]__[local]___[hash:base64:5]'
                }
            },
            {
                test: /\.styl$/,
                loader: 'stylus-loader'
            },
            {
                test: /\.(jpg|png)$/i,
                loader: 'file-loader'
            }
        ]
    },
    plugins: [HtmlWebpackPluginConfig]
};
